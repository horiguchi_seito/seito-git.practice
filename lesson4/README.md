デザイナー向けのGit講座 #4

##テーマ: リモートリポジトリを理解

##1. フォーク

##2. プルリクエスト

##宿題
###このリポジトリをフォークして、プルリクエストを送ってみましょう。
(mynameは自分のアカウント名など適宜変更して下さい)

* n0bisuke/git.practiceリポジトリをフォークしてmyname/git.practiceを作る
* myname/git.practiceをcloneしてローカルリポジトリ作成
* myname/git.practiceのローカルリポジトリ上でlesson4ブランチを作成
* lesson4ブランチでlesson4フォルダ内の`home_workフォルダ`内にmyname.htmlを作成
* myname/git.practiceのリモートリポジトリへpush
* myname/git.practice:lesson4 から n0bisuke/git.practice:master へプルokuxtutekudasai
* 最終的に n0bisuke/git.practice:masterの`lesson4/home_work`フォルダにメンバー分のmyname.htmlが入る予定です。

